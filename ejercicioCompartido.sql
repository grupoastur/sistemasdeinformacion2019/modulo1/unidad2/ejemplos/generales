﻿USE concursos;

-- 1 indicar el nombre de los concursantes cuya altura sea 170cm o mas
SELECT c.nombre, c.altura FROM concursantes c WHERE c.altura>=170; 

-- 2 indicar los pesos que se repiten mas de una vez
  SELECT c.peso FROM concursantes c GROUP BY c.peso HAVING COUNT(*)>1; 

  -- 3 indicar las poblaciones con mas de 3 concursantes que hayan nacido despues de 1983
    SELECT c.poblacion FROM concursantes c WHERE YEAR( c.fechaNacimiento)>1983 GROUP BY c.poblacion HAVING COUNT(*)>3;

-- 4 indicar la altura media y el peso maximo de los concursantes por provincia
  SELECT c.provincia, AVG( c.altura) alturaMedia, MAX( c.peso) pesoMaximo FROM concursantes c GROUP BY c.provincia; 

-- 5 indicar la provincia que tiene mas de 5 concursantes de peso entre 95 y 105 y mostrar tanbien el peso maximo y minimo
  SELECT c.provincia, c.peso, MAX( c.peso), MIN( c.peso) FROM concursantes c WHERE c.peso BETWEEN 95 AND 105 GROUP BY c.provincia HAVING COUNT(*)>=5; 