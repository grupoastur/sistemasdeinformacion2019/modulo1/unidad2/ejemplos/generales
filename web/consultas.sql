﻿USE paginaweb;

-- 1 indicar el nombre del autor y el título del libro
  SELECT a.nombre_completo nombre_de_autor, l.titulo titulo_del_libro FROM autor a JOIN libro l ON a.id_autor = l.autor ;
  
-- 2 indicar el nombre de los autores y el título en el que han ayudado
  SELECT l.titulo, a1.nombre_completo   FROM libro l JOIN ayuda a ON l.id_libro = a.libro JOIN autor a1 ON a.autor = a1.id_autor ;
  
-- 3 indicar los libros que se ha descargado cada usuario con la fecha de descarga. listar el login, la fecha de descarga y el id del libro 
  SELECT * FROM fechadescarga f;  
  
-- 4 indicar los libros que se ha descargado cada usuario con la fecha de descarga. listar el login, la fecha de descarga, el titulo del libro y correo
  SELECT u.login, f.fecha_descarga, l.titulo, u.email FROM fechadescarga f JOIN descarga d ON f.libro = d.libro AND f.usuario = d.usuario JOIN usuario u ON d.usuario = u.login JOIN libro l ON d.libro = l.id_libro; 
  SELECT l.titulo, u.login, f.fecha_descarga, u.email  FROM fechadescarga f JOIN libro l JOIN usuario u JOIN descarga d ON f.libro = d.libro AND f.usuario = d.usuario AND u.login = d.usuario AND l.id_libro = d.libro; 

-- 5 indicar el número de libros que hay
  SELECT COUNT(*) FROM libro l;

-- 6 indicar el número de libros por colección (no hace falta colocar nombre de la colección)
  SELECT COUNT(*) nlibros, l.coleccion FROM libro l GROUP BY l.coleccion; 
  
-- 7 indicar la colección que tiene mas libros (no hacie falta colocar nombre de la colección)
   -- subconsulta c1
  SELECT l.coleccion,COUNT(*) nlibros FROM libro l GROUP BY l.coleccion;
  -- subconsulta c2
    SELECT MAX(nlibros) maximo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1;
    -- combinacion final
  SELECT c1.coleccion FROM (SELECT coleccion,COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1 
  JOIN (SELECT MAX(nlibros) maximo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1) c2 ON c1.nlibros=c2.maximo;
  
-- 8 indicar la colección que tiene menos libros (no hace falta colocar nombre de la colección)
  -- subconsulta c1
  SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion;
  -- subconsulta c2
    SELECT MIN (c1.nlibros) minimo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1 ;
  -- consulta final
    SELECT c1.coleccion FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1 
      JOIN (SELECT MIN(c1.nlibros) minimo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1) c2 ON c1.nlibros= c2.minimo;   

 
-- 9 indicar el nombre de la colección que tiene mas libros
  
  SELECT c.nombre FROM coleccion c
    JOIN (
      SELECT c1.coleccion FROM (SELECT coleccion,COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1 
        JOIN (SELECT MAX(nlibros) maximo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1) c2 ON c1.nlibros=c2.maximo
    )consulta7
    ON c.id_coleccion=consulta7.coleccion;

  
-- 10 indicar el nombre de la colección que tiene menos libros
 SELECT c.nombre FROM coleccion c
    JOIN (
      SELECT c1.coleccion FROM (SELECT coleccion,COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1 
        JOIN (SELECT MIN(nlibros) maximo FROM (SELECT l.coleccion, COUNT(*) nlibros FROM libro l GROUP BY l.coleccion) c1) c2 ON c1.nlibros=c2.maximo
    )consulta7
    ON c.id_coleccion=consulta7.coleccion;
  
  
-- 11 indicar el nombre del libro que se ha descargado mas veces
  -- c1
  SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro;
 -- c2  
  SELECT MAX(c1.ndescargas) maximo FROM (SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1;

  -- c3 
  SELECT c1.libro FROM (SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1 
  JOIN (SELECT MAX( c1.ndescargas) maximo FROM (SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1) c2 ON c1.ndescargas= c2.maximo ;

  -- final
  SELECT l.titulo FROM libro l JOIN (
    SELECT c1.libro FROM (SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1 
    JOIN (SELECT MAX( c1.ndescargas) maximo FROM (SELECT COUNT(*) ndescargas, f.libro FROM fechadescarga f GROUP BY f.libro) c1) c2 ON c1.ndescargas= c2.maximo
  ) c3
  ON c3.libro=l.id_libro;

-- 12 indicar el nombre del usuario que ha descargado mas libros
  -- c1
  SELECT COUNT(*) ndescarga, f.usuario FROM usuario u, fechadescarga f GROUP BY f.usuario;
  
  -- c2
  SELECT MAX( c2.ndescarga) maximo FROM ( SELECT COUNT(*) ndescarga, f.usuario FROM usuario u, fechadescarga f GROUP BY f.usuario) c2;
  
  -- cfinal
  SELECT c1.usuario FROM (SELECT COUNT(*) ndescarga, f.usuario FROM usuario u, fechadescarga f GROUP BY f.usuario) c1
    JOIN (SELECT MAX( c2.ndescarga) maximo FROM ( SELECT COUNT(*) ndescarga, f.usuario FROM usuario u, fechadescarga f GROUP BY f.usuario) c2) c2
    ON c1.ndescarga= c2.maximo; 
  
  -- con havin
  -- c1
  -- numero de descargas por usuario
    SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario ;
  -- c2
    SELECT MAX( c1.ndescargas) maximo FROM (SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario) c1; 
  -- final con join
    SELECT c1.usuario FROM(SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario) c1 
    JOIN (SELECT MAX( c1.ndescargas) maximo FROM (SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario) c1) c2 ON c1.ndescargas= c2.maximo;
   -- final con havin
   SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario 
    HAVING ndescargas= (SELECT MAX( c1.ndescargas) maximo FROM (SELECT f.usuario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario) c1);                
  
  
-- 13 indicar el nombre de los usuarios que han descargado mas libros que Adam3
 -- c1 
   SELECT COUNT(*) ndescargas FROM fechadescarga f WHERE f.usuario='Adam3';
-- c2 
  SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario;
-- cfinal
  SELECT c2.usuario FROM (SELECT COUNT(*) ndescarga, f.usuario FROM fechadescarga f GROUP BY f.usuario) c2
  WHERE c2.ndescarga> (SELECT COUNT(*) ndescargas FROM fechadescarga f WHERE f.usuario='Adam3');  
  
-- 14 indicar el mes que mas libros se han descargado
  -- c1
  SELECT COUNT(*) numdescarga, month(f.fecha_descarga) mes FROM fechadescarga f GROUP BY MONTH( f.fecha_descarga);
  -- c2
  SELECT MAX( c1.numdescarga) FROM (SELECT COUNT(*) numdescarga, month(f.fecha_descarga) mes FROM fechadescarga f GROUP BY MONTH( f.fecha_descarga)) c1; 
  -- final con join
SELECT c1.mes FROM (SELECT COUNT(*) numdescarga, month(f.fecha_descarga) mes FROM fechadescarga f GROUP BY MONTH( f.fecha_descarga)) c1 
JOIN ( SELECT MAX( c1.numdescarga) maximo FROM (SELECT COUNT(*) numdescarga, month(f.fecha_descarga) mes FROM fechadescarga f GROUP BY MONTH( f.fecha_descarga)) c1) c2 
  ON c1.numdescarga= c2.maximo;
-- final con havin
SELECT * FROM              
  
-- 15                        
     